package ictgradschool.industry.lab04.ex04;

import ictgradschool.Keyboard;

/**
 A game of Rock, Paper Scissors
 */
public class RockPaperScissors {

    public static final int ROCK = 1;
    // TODO Make similar constants for PAPER and SCISSORS, to improve readability of your code.
    public static final int PAPER = 2;
    public static final int SCISSORS = 3;
    String[] gameArrays = new String[] {"rock", "paper", "scissors"};

    public void start() {

        // TODO Write your code here which calls your other methods in order to play the game. Implement this
        // as detailed in the exercise sheet.
        System.out.println("Hi! What's your name?");
        String name = Keyboard.readInput();

        // player choose a number
        int playerChoice = 0;
        System.out.println("1. ROCK\n2. PAPER\n3. SCISSORS\n4. Quit");
        System.out.println("Enter your choice:");
        playerChoice = Integer.parseInt(Keyboard.readInput());
        if (playerChoice == 4) {
            System.out.println("Goodbye " + name+ ". Thanks for playing :)");
        }
        else {
            int computerChoice = (int)(Math.random() * (3 - 1 + 1)) +1;
            displayPlayerChoice(name, playerChoice);
            displayPlayerChoice("Computer", computerChoice);
            if (computerChoice == playerChoice) {
                System.out.println("No one wins.");
            }
            else if (userWins(playerChoice,computerChoice)) {
                System.out.println(name + " wins because " + getResultString(playerChoice,computerChoice));
            }
            else {
                System.out.println("The computer wins because " + getResultString(playerChoice,computerChoice));
            }
        }

    }

    public void displayPlayerChoice(String name, int choice) {
        // TODO This method should print out a message stating that someone chose a particular thing (rock, paper or scissors)
        System.out.println(name + " chose " + gameArrays[choice - 1]);
    }

    public boolean userWins(int playerChoice, int computerChoice) {
        // TODO Determine who wins and return true if the player won, false otherwise.
        if (playerChoice == ROCK && computerChoice == SCISSORS) {
            return true;
        }
        else if (playerChoice == SCISSORS && computerChoice == PAPER) {
            return true;
        }
        else if (playerChoice == PAPER && computerChoice == ROCK) {
            return true;
        }

        return false;
    }

    public String getResultString(int playerChoice, int computerChoice) {

        final String PAPER_WINS = "paper covers rock";
        final String ROCK_WINS = "rock smashes scissors";
        final String SCISSORS_WINS = "scissors cut paper";
        final String TIE = " you chose the same as the computer";

        // TODO Return one of the above messages depending on what playerChoice and computerChoice are.
        if (playerChoice == computerChoice) {
            return TIE;
        }
        else if (playerChoice == PAPER && computerChoice == ROCK || playerChoice == ROCK && computerChoice == PAPER) {
            return PAPER_WINS;
        }
        else if (playerChoice == PAPER && computerChoice == SCISSORS || playerChoice == SCISSORS && computerChoice == PAPER) {
            return SCISSORS_WINS;
        }
        else {
            return ROCK_WINS;
        }

    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        RockPaperScissors ex = new RockPaperScissors();
        ex.start();

    }
}
