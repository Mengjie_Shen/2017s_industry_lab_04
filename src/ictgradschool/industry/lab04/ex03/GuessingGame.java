package ictgradschool.industry.lab04.ex03;

import ictgradschool.Keyboard;

/**
 * A guessing game!
 */
public class GuessingGame {

    public void start() {

        // TODO Write your code here.
        int goal = (int) (Math.random() * (100 - 1 + 1)) + 1;
        int guess = 0;
        System.out.printf("Enter the number you guess:");
        guess = Integer.parseInt(Keyboard.readInput());
        while (guess != goal) {
            if (guess > goal) {
                System.out.printf("Too high, try again");
                System.out.printf("Enter the number you guess:");
                guess = Integer.parseInt(Keyboard.readInput());
            }
            else {
                System.out.printf("Too low, try again");
                System.out.printf("Enter the number you guess:");
                guess = Integer.parseInt(Keyboard.readInput());
            }
        }
        System.out.println("Perfect!!");
        System.out.printf("Goodbye");
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        GuessingGame ex = new GuessingGame();
        ex.start();

    }
}
