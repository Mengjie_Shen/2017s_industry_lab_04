package ictgradschool.industry.lab04.ex05;

/**
 * Created by mshe666 on 14/11/2017.
 */
public class Pattern {
    // Declare instance variables:
    private int numberOfCharacters;
    private char symbolPattern;

    public Pattern(int numberOfCharacters, char symbolPattern) {
        this.numberOfCharacters = numberOfCharacters;
        this.symbolPattern = symbolPattern;
    }

    public int getNumberOfCharacters() {
        return numberOfCharacters;
    }

    public void setNumberOfCharacters(int numberOfCharacters) {
        this.numberOfCharacters = numberOfCharacters;
    }

    public String toString() {
        String info = "";
        for (int i = 0; i < numberOfCharacters; i++) {
            info += "" + symbolPattern;
        }
        return info;
    }



}
