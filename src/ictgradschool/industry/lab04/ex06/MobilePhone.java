package ictgradschool.industry.lab04.ex06;

import java.util.Objects;

public class MobilePhone {

    // TODO Declare the 3 instance variables:
    // brand
    private String brand;
    // model
    private String model;
    // price
    private double price;
    
    public MobilePhone(String brand, String model, double price) {
        // Complete this constructor method
        this.brand = brand;
        this.model = model;
        this.price = price;
    }

    // TODO Uncomment these methods once the corresponding instance variable has been declared.
    public String getBrand() {
        return brand;
    }
    
    public void setBrand(String brand) {
        this.brand = brand;
    }
    
    // TODO Insert getModel() method here
    public String getModel() {
        return model;
    }
    
    // TODO Insert setModel() method here
    public void setModel(String model) {
        this.model = model;
    }
    
    // TODO Insert getPrice() method here
    public double getPrice() {
        return price;
    }
    
    // TODO Insert setPrice() method here
    public void setPrice(double price) {
        this.price = price;
    }
    
    // TODO Insert toString() method here
    public String toString() {
        String info = "";
        info += brand + " " + model + " which costs $" + price;
        return info;
    }

    
    // TODO Insert isCheaperThan() method here
    public boolean isCheaperThan(MobilePhone other) {
        double otherPrice = other.price;
        return this.price < otherPrice;
    }
    
    // TODO Insert equals() method here
    public boolean equals(MobilePhone other) {
        if (other instanceof MobilePhone) {
            MobilePhone otherM = (MobilePhone) other;
            return this.brand.equals(otherM.brand) && this.model.equals(otherM.model) && this.price == otherM.price;
        }
        else {
            return false;
        }
    }

}


